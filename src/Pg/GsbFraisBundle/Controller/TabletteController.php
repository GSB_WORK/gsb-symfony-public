<?php
namespace Pg\GsbFraisBundle\Controller;
require_once("include/fct.inc.php");
//require_once ("include/class.pdogsb.inc.php");
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use PdoGsb;

class TabletteController extends Controller
{
    public function creerTabletteAction()
    {
        $session= $this->get('request')->getSession();
        $idVisiteur =  $session->get('id');
        $mois = getMois(date("d/m/Y"));
        $numAnnee =substr( $mois,0,4);
        $numMois =substr( $mois,4,2);
//        $pdo = PdoGsb::getPdoGsb();

        if($this->get('request')->getMethod() == 'POST')
        {
            $pdo = $this->get('pg_gsb_frais.pdo');
            $os = $_POST['os'];
            $memoireint = $_POST['memoireinterne'];
            if(isset($_POST['memoireext']))
            {
            $memoireext = $_POST['memoireext'];
            }
            else
            {
            $memoireext = NULL;
            }
            $tablette = $pdo->ajouterTablette($os, $memoireext, $memoireint);
            $session->getFlashBag()->add('info', 'Tablette bien enregistrée');
            return $this->render('PgGsbFraisBundle:Tablette:creertablette.html.twig');

        }
        else
        {
            return $this->render('PgGsbFraisBundle:Tablette:creertablette.html.twig');
        }
     }

     public function affecterTabletteAction()
     {
        $session= $this->get('request')->getSession();
        $idVisiteur =  $session->get('id');
        $mois = getMois(date("d/m/Y"));
        $numAnnee =substr( $mois,0,4);
        $numMois =substr( $mois,4,2);
        $pdo = $this->get('pg_gsb_frais.pdo');
        $utilisateur = $pdo->getUtilisateur();
        $tablette = $pdo->getTabletteDispo();

        if($this->get('request')->getMethod() == 'POST')
        {

        }

        else
        {

            return $this->render('PgGsbFraisBundle:Tablette:affectertablette.html.twig', array('user'=>$utilisateur, 'tablette'=>$tablette));
        }
     }
}