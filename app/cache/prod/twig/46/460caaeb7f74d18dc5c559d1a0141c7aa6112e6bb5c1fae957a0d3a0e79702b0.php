<?php

/* PgGsbFraisBundle::layout.html.twig */
class __TwigTemplate_94359d6e779550172bf966373431efbaecb7e290b3a9ab6fb15969db65714b1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
            'body' => array($this, 'block_body'),
            'bloc1' => array($this, 'block_bloc1'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"
       \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"fr\" lang=\"fr\">
  <head>
    <title>Intranet du Laboratoire Galaxy-Swiss Bourdin</title>
    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />
    <link href=\"http://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/gsbfrais/css/styles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/gsbfrais/css/materialize.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css\" />
    <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css\" rel=\"stylesheet\" integrity=\"sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1\" crossorigin=\"anonymous\">

    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"./images/favicon.ico\" />
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>

  </head>
  <body>
<header>
    <div class=\"nav-wrapper\">

<ul id=\"slide-out\" class=\"side-nav fixed center-align\">
 <img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/gsbfrais/images/logo.jpg"), "html", null, true);
        echo "\" id=\"logoGSB\" alt=\"Laboratoire Galaxy-Swiss Bourdin\" title=\"Laboratoire Galaxy-Swiss Bourdin\" />
          ";
        // line 24
        $this->displayBlock('menu', $context, $blocks);
        // line 26
        echo "      </ul>


</div>
<nav class=\"blue darken-1 nav-gsb center-align\">
<a href=\"#\" data-activates=\"slide-out\" class=\"button-collapse\"><i class=\"material-icons\">menu</i></a>
        <span class=\"top-title\">Suivi du remboursement des frais</span>
</nav>
</header>
<main>

          ";
        // line 37
        $this->displayBlock('body', $context, $blocks);
        // line 41
        echo "     
     </main>
          <script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-2.1.1.min.js\"></script>
      <script type=\"text/javascript\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/gsbfrais/js/materialize.min.js"), "html", null, true);
        echo "\"></script>
      <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js\"></script>
            ";
        // line 46
        if (array_key_exists("message", $context)) {
            // line 47
            echo "            <script>
              \$(document).ready(function(){
swal(\"Erreur\", \"";
            // line 49
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
            echo "\", \"error\");
});
            </script>
      ";
        }
        // line 53
        echo "   <script>
                 \$(document).ready(function(){
       \$(\".button-collapse\").sideNav();
});

</script>
   
    </body>
  </html>

          
          
";
    }

    // line 24
    public function block_menu($context, array $blocks = array())
    {
        // line 25
        echo "          ";
    }

    // line 37
    public function block_body($context, array $blocks = array())
    {
        echo " 
                 ";
        // line 38
        $this->displayBlock('bloc1', $context, $blocks);
        // line 40
        echo "          ";
    }

    // line 38
    public function block_bloc1($context, array $blocks = array())
    {
        // line 39
        echo "                 ";
    }

    public function getTemplateName()
    {
        return "PgGsbFraisBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 39,  130 => 38,  126 => 40,  124 => 38,  119 => 37,  115 => 25,  112 => 24,  96 => 53,  89 => 49,  85 => 47,  83 => 46,  78 => 44,  73 => 41,  71 => 37,  58 => 26,  56 => 24,  52 => 23,  35 => 9,  31 => 8,  22 => 1,);
    }
}
/* <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"*/
/*        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">*/
/* <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">*/
/*   <head>*/
/*     <title>Intranet du Laboratoire Galaxy-Swiss Bourdin</title>*/
/*     <meta http-equiv="content-type" content="text/html; charset=utf-8" />*/
/*     <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">*/
/*     <link href="{{ asset('bundles/gsbfrais/css/styles.css') }}" rel="stylesheet" type="text/css" />*/
/*     <link href="{{ asset('bundles/gsbfrais/css/materialize.min.css') }}" rel="stylesheet" type="text/css" />*/
/*     <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">*/
/*     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />*/
/*     <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">*/
/* */
/*     <link rel="shortcut icon" type="image/x-icon" href="./images/favicon.ico" />*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0"/>*/
/* */
/*   </head>*/
/*   <body>*/
/* <header>*/
/*     <div class="nav-wrapper">*/
/* */
/* <ul id="slide-out" class="side-nav fixed center-align">*/
/*  <img src="{{ asset('bundles/gsbfrais/images/logo.jpg')}}" id="logoGSB" alt="Laboratoire Galaxy-Swiss Bourdin" title="Laboratoire Galaxy-Swiss Bourdin" />*/
/*           {% block menu %}*/
/*           {% endblock %}*/
/*       </ul>*/
/* */
/* */
/* </div>*/
/* <nav class="blue darken-1 nav-gsb center-align">*/
/* <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>*/
/*         <span class="top-title">Suivi du remboursement des frais</span>*/
/* </nav>*/
/* </header>*/
/* <main>*/
/* */
/*           {% block body %} */
/*                  {% block bloc1 %}*/
/*                  {% endblock %}*/
/*           {% endblock %}*/
/*      */
/*      </main>*/
/*           <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>*/
/*       <script type="text/javascript" src="{{ asset('bundles/gsbfrais/js/materialize.min.js') }}"></script>*/
/*       <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>*/
/*             {%if(message is defined )%}*/
/*             <script>*/
/*               $(document).ready(function(){*/
/* swal("Erreur", "{{message}}", "error");*/
/* });*/
/*             </script>*/
/*       {%endif%}*/
/*    <script>*/
/*                  $(document).ready(function(){*/
/*        $(".button-collapse").sideNav();*/
/* });*/
/* */
/* </script>*/
/*    */
/*     </body>*/
/*   </html>*/
/* */
/*           */
/*           */
/* */
