<?php

/* PgGsbFraisBundle:Home:connexion.html.twig */
class __TwigTemplate_6f72220b261d78c352e4529866d643af18ed2daaf60c2997047600f75aa73d2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PgGsbFraisBundle::layout.html.twig", "PgGsbFraisBundle:Home:connexion.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'bloc1' => array($this, 'block_bloc1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PgGsbFraisBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo " ";
        $this->displayBlock('bloc1', $context, $blocks);
        // line 23
        echo "    
    ";
    }

    // line 3
    public function block_bloc1($context, array $blocks = array())
    {
        echo " 
 <div id=\"contenu\">
 <div class=\"row\">
      <div class=\"col m6 offset-m3\">
<form method=\"POST\" action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_validerconnexion");
        echo "\" id=\"form-login\">
        <p>
          <label for=\"nom\">Login :</label>
           <input placeholder=\"Identifiant...\" id=\"login\" type=\"text\" name=\"login\"  size=\"30\" maxlength=\"45\">
        </p>
\t<p>
            <label for=\"mdp\">Mot de passe :</label>
            <input placeholder=\"Mot de passe...\" id=\"mdp\"  type=\"password\"  name=\"mdp\" size=\"30\" maxlength=\"45\">
        </p>
         <input class=\"waves-effect waves-light btn blue\" type=\"submit\" value=\"Valider\" name=\"valider\">
         <input class=\"waves-effect waves-light btn blue\" type=\"reset\" value=\"Annuler\" name=\"annuler\"> 
      </p>
</form>
</div>
</div>
</div>
    ";
    }

    public function getTemplateName()
    {
        return "PgGsbFraisBundle:Home:connexion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 7,  40 => 3,  35 => 23,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends "PgGsbFraisBundle::layout.html.twig" %}*/
/* {% block body %}*/
/*  {% block bloc1 %} */
/*  <div id="contenu">*/
/*  <div class="row">*/
/*       <div class="col m6 offset-m3">*/
/* <form method="POST" action="{{path('pg_gsb_frais_validerconnexion')}}" id="form-login">*/
/*         <p>*/
/*           <label for="nom">Login :</label>*/
/*            <input placeholder="Identifiant..." id="login" type="text" name="login"  size="30" maxlength="45">*/
/*         </p>*/
/* 	<p>*/
/*             <label for="mdp">Mot de passe :</label>*/
/*             <input placeholder="Mot de passe..." id="mdp"  type="password"  name="mdp" size="30" maxlength="45">*/
/*         </p>*/
/*          <input class="waves-effect waves-light btn blue" type="submit" value="Valider" name="valider">*/
/*          <input class="waves-effect waves-light btn blue" type="reset" value="Annuler" name="annuler"> */
/*       </p>*/
/* </form>*/
/* </div>*/
/* </div>*/
/* </div>*/
/*     {% endblock %}    */
/*     {% endblock %}*/
/* */
/*      */
