<?php

/* PgGsbFraisBundle:SaisirFrais:saisirfraishorsforfait.html.twig */
class __TwigTemplate_8e3a8574a6e1e8a0f077dafc484183ef180b7b2f17d1effdf9d347cb68c92ea2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"contenu\">
<table class=\"listeLegere\">
  \t   <caption>Descriptif des éléments hors forfait
       </caption>
             <tr>
                <th class=\"date\">Date</th>
\t\t\t\t<th class=\"libelle\">Libellé</th>  
                <th class=\"montant\">Montant</th>  
                <th class=\"action\">&nbsp;</th>              
             </tr>
 \t";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lesfraishorsforfait"]) ? $context["lesfraishorsforfait"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["unfrais"]) {
            // line 12
            echo "             ";
            $context["libelle"] = $this->getAttribute($context["unfrais"], "libelle", array());
            // line 13
            echo "             ";
            $context["date"] = $this->getAttribute($context["unfrais"], "date", array());
            // line 14
            echo "             ";
            $context["montant"] = $this->getAttribute($context["unfrais"], "montant", array());
            // line 15
            echo "             ";
            $context["idfrais"] = $this->getAttribute($context["unfrais"], "id", array());
            // line 16
            echo "             <tr>
                <td>";
            // line 17
            echo twig_escape_filter($this->env, (isset($context["date"]) ? $context["date"] : null), "html", null, true);
            echo "</td>
                <td>";
            // line 18
            echo twig_escape_filter($this->env, (isset($context["libelle"]) ? $context["libelle"] : null), "html", null, true);
            echo "</td>
                <td>";
            // line 19
            echo twig_escape_filter($this->env, (isset($context["montant"]) ? $context["montant"] : null), "html", null, true);
            echo "</td>
                <td><a href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pg_gsb_frais_supprimerfraishorsforfait", array("id" => (isset($context["idfrais"]) ? $context["idfrais"] : null))), "html", null, true);
            echo "\" 
\t\t\t\tonclick=\"return confirm('Voulez-vous vraiment supprimer ce frais?');\">Supprimer ce frais</a></td>
             </tr>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['unfrais'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "  
                                          
    </table>
      <form action=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_validerfraishorsforfais");
        echo "\" method=\"post\">
      <div class=\"corpsForm\">
         
          <fieldset>
            <legend>Nouvel élément hors forfait
            </legend>
            <p>
              <label for=\"txtDateHF\">Date (jj/mm/aaaa): </label>
              <input type=\"text\" id=\"txtDateHF\" name=\"dateFrais\" size=\"10\" maxlength=\"10\" value=\"\"  />
            </p>
            <p>
              <label for=\"txtLibelleHF\">Libellé</label>
              <input type=\"text\" id=\"txtLibelleHF\" name=\"libelle\" size=\"70\" maxlength=\"256\" value=\"\" />
            </p>
            <p>
              <label for=\"txtMontantHF\">Montant : </label>
              <input type=\"text\" id=\"txtMontantHF\" name=\"montant\" size=\"10\" maxlength=\"10\" value=\"\" />
            </p>
          </fieldset>
      </div>
      <div class=\"piedForm\">
      <p>
        <input id=\"ajouter\" type=\"submit\" value=\"Ajouter\" size=\"20\" />
        <input id=\"effacer\" type=\"reset\" value=\"Effacer\" size=\"20\" />
      </p> 
      </div>
        
      </form>
</div>";
    }

    public function getTemplateName()
    {
        return "PgGsbFraisBundle:SaisirFrais:saisirfraishorsforfait.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 26,  72 => 23,  62 => 20,  58 => 19,  54 => 18,  50 => 17,  47 => 16,  44 => 15,  41 => 14,  38 => 13,  35 => 12,  31 => 11,  19 => 1,);
    }
}
/* <div id="contenu">*/
/* <table class="listeLegere">*/
/*   	   <caption>Descriptif des éléments hors forfait*/
/*        </caption>*/
/*              <tr>*/
/*                 <th class="date">Date</th>*/
/* 				<th class="libelle">Libellé</th>  */
/*                 <th class="montant">Montant</th>  */
/*                 <th class="action">&nbsp;</th>              */
/*              </tr>*/
/*  	{% for unfrais in  lesfraishorsforfait %}*/
/*              {% set libelle = unfrais.libelle %}*/
/*              {% set date = unfrais.date %}*/
/*              {% set montant = unfrais.montant %}*/
/*              {% set idfrais = unfrais.id %}*/
/*              <tr>*/
/*                 <td>{{date}}</td>*/
/*                 <td>{{libelle}}</td>*/
/*                 <td>{{montant}}</td>*/
/*                 <td><a href="{{path('pg_gsb_frais_supprimerfraishorsforfait',{'id' : idfrais})}}" */
/* 				onclick="return confirm('Voulez-vous vraiment supprimer ce frais?');">Supprimer ce frais</a></td>*/
/*              </tr>*/
/* 	{%endfor%}  */
/*                                           */
/*     </table>*/
/*       <form action="{{path('pg_gsb_frais_validerfraishorsforfais')}}" method="post">*/
/*       <div class="corpsForm">*/
/*          */
/*           <fieldset>*/
/*             <legend>Nouvel élément hors forfait*/
/*             </legend>*/
/*             <p>*/
/*               <label for="txtDateHF">Date (jj/mm/aaaa): </label>*/
/*               <input type="text" id="txtDateHF" name="dateFrais" size="10" maxlength="10" value=""  />*/
/*             </p>*/
/*             <p>*/
/*               <label for="txtLibelleHF">Libellé</label>*/
/*               <input type="text" id="txtLibelleHF" name="libelle" size="70" maxlength="256" value="" />*/
/*             </p>*/
/*             <p>*/
/*               <label for="txtMontantHF">Montant : </label>*/
/*               <input type="text" id="txtMontantHF" name="montant" size="10" maxlength="10" value="" />*/
/*             </p>*/
/*           </fieldset>*/
/*       </div>*/
/*       <div class="piedForm">*/
/*       <p>*/
/*         <input id="ajouter" type="submit" value="Ajouter" size="20" />*/
/*         <input id="effacer" type="reset" value="Effacer" size="20" />*/
/*       </p> */
/*       </div>*/
/*         */
/*       </form>*/
/* </div>*/
