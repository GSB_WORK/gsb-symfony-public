<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_06ed8a935e8308a12c9a258046514f65bd041b8d1ae74698cbc8b0aeac961ccf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_89e8455c0d981595ade793adc968c165303c73790e36916bacb049770fafced5 = $this->env->getExtension("native_profiler");
        $__internal_89e8455c0d981595ade793adc968c165303c73790e36916bacb049770fafced5->enter($__internal_89e8455c0d981595ade793adc968c165303c73790e36916bacb049770fafced5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_89e8455c0d981595ade793adc968c165303c73790e36916bacb049770fafced5->leave($__internal_89e8455c0d981595ade793adc968c165303c73790e36916bacb049770fafced5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
