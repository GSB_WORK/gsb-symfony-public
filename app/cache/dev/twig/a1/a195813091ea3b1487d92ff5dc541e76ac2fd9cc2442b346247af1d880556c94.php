<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_929fe872152d860f30065458722333c4331cf25230944a6655659166d65b4bb4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_294e78970d73139e860aa1578205b3501e98ac61670d39ffb77350eb55ad6c2b = $this->env->getExtension("native_profiler");
        $__internal_294e78970d73139e860aa1578205b3501e98ac61670d39ffb77350eb55ad6c2b->enter($__internal_294e78970d73139e860aa1578205b3501e98ac61670d39ffb77350eb55ad6c2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_294e78970d73139e860aa1578205b3501e98ac61670d39ffb77350eb55ad6c2b->leave($__internal_294e78970d73139e860aa1578205b3501e98ac61670d39ffb77350eb55ad6c2b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
