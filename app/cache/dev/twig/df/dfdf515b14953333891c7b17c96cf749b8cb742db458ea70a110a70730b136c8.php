<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_6ae6d763c5b7ed3c2a7eb7cc3d418f9c7cb77bc21f37c0f112ead00c98171833 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4c72ff1108b65f2ba7c533d925bb60efbddcba30904570dde61a2e00a98f1ba0 = $this->env->getExtension("native_profiler");
        $__internal_4c72ff1108b65f2ba7c533d925bb60efbddcba30904570dde61a2e00a98f1ba0->enter($__internal_4c72ff1108b65f2ba7c533d925bb60efbddcba30904570dde61a2e00a98f1ba0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_4c72ff1108b65f2ba7c533d925bb60efbddcba30904570dde61a2e00a98f1ba0->leave($__internal_4c72ff1108b65f2ba7c533d925bb60efbddcba30904570dde61a2e00a98f1ba0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
