<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_1fe7befdafa392a1791ae40147fcc6f7320d9efd5ef7274e7bd4d334160002ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2db403b859b5f5cf39f3909ac4a346baeea03e0ee9039068c5163784925bea8a = $this->env->getExtension("native_profiler");
        $__internal_2db403b859b5f5cf39f3909ac4a346baeea03e0ee9039068c5163784925bea8a->enter($__internal_2db403b859b5f5cf39f3909ac4a346baeea03e0ee9039068c5163784925bea8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_2db403b859b5f5cf39f3909ac4a346baeea03e0ee9039068c5163784925bea8a->leave($__internal_2db403b859b5f5cf39f3909ac4a346baeea03e0ee9039068c5163784925bea8a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
