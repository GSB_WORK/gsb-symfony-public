<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_6d7be1346073553e5040ea9ff52761f69579cc3230ee02d1e035a76dd191beee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb80778a9e9ef2b93a0c9cef90c063aac19b60ec8ca7538d0fd642c41746a887 = $this->env->getExtension("native_profiler");
        $__internal_cb80778a9e9ef2b93a0c9cef90c063aac19b60ec8ca7538d0fd642c41746a887->enter($__internal_cb80778a9e9ef2b93a0c9cef90c063aac19b60ec8ca7538d0fd642c41746a887_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_cb80778a9e9ef2b93a0c9cef90c063aac19b60ec8ca7538d0fd642c41746a887->leave($__internal_cb80778a9e9ef2b93a0c9cef90c063aac19b60ec8ca7538d0fd642c41746a887_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
