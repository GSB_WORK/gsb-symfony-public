<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_86ed8f28fc3256f4e34577e520c2c41dcf7ee743e7b3513a72b4ebd8b5b4d1fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ae50cce3dfc90d36a291f5b6193439c251d1e85093824e2b61e144e875b08ff = $this->env->getExtension("native_profiler");
        $__internal_8ae50cce3dfc90d36a291f5b6193439c251d1e85093824e2b61e144e875b08ff->enter($__internal_8ae50cce3dfc90d36a291f5b6193439c251d1e85093824e2b61e144e875b08ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_8ae50cce3dfc90d36a291f5b6193439c251d1e85093824e2b61e144e875b08ff->leave($__internal_8ae50cce3dfc90d36a291f5b6193439c251d1e85093824e2b61e144e875b08ff_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_rows') ?>*/
/* */
