<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_9b0b0d8ee21057b253a28a78bdcbca1e21150b4d17b7199237089312b609b5d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f29774d0f06b93148a13948fe369328fa8f05c498fa5ef3a71070181346731a4 = $this->env->getExtension("native_profiler");
        $__internal_f29774d0f06b93148a13948fe369328fa8f05c498fa5ef3a71070181346731a4->enter($__internal_f29774d0f06b93148a13948fe369328fa8f05c498fa5ef3a71070181346731a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_f29774d0f06b93148a13948fe369328fa8f05c498fa5ef3a71070181346731a4->leave($__internal_f29774d0f06b93148a13948fe369328fa8f05c498fa5ef3a71070181346731a4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
