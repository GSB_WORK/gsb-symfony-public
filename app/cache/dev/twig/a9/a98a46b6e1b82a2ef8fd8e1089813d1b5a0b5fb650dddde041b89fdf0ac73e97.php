<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_88e908cb9c702f096f7797e0cc6ab1351c1270c7226feb993445d73fce86a5a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b24b2b865ee686957bb2f29dbb807c63f61f79e857618e14d41c948d58cbe2a8 = $this->env->getExtension("native_profiler");
        $__internal_b24b2b865ee686957bb2f29dbb807c63f61f79e857618e14d41c948d58cbe2a8->enter($__internal_b24b2b865ee686957bb2f29dbb807c63f61f79e857618e14d41c948d58cbe2a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_b24b2b865ee686957bb2f29dbb807c63f61f79e857618e14d41c948d58cbe2a8->leave($__internal_b24b2b865ee686957bb2f29dbb807c63f61f79e857618e14d41c948d58cbe2a8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
