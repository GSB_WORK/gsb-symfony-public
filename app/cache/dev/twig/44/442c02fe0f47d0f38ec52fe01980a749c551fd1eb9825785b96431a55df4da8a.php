<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_e552d0755d7c6df31ddf46adc12432ea2c61f518abb29155af04cd119d4005d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a15e8cb3d89227e5ada2170525b4e75e7aa5fcbfa569e0bbd8080e3f403ea64 = $this->env->getExtension("native_profiler");
        $__internal_1a15e8cb3d89227e5ada2170525b4e75e7aa5fcbfa569e0bbd8080e3f403ea64->enter($__internal_1a15e8cb3d89227e5ada2170525b4e75e7aa5fcbfa569e0bbd8080e3f403ea64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_1a15e8cb3d89227e5ada2170525b4e75e7aa5fcbfa569e0bbd8080e3f403ea64->leave($__internal_1a15e8cb3d89227e5ada2170525b4e75e7aa5fcbfa569e0bbd8080e3f403ea64_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
