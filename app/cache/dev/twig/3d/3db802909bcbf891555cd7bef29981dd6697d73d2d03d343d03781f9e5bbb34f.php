<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_cf168fda8bf19e973b7955c2b00c52b75bd57cff5a09a50494b6f172417bde3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a8b659d5416d33e4b5b8cd22a943708708487af36d6b17c2c1119153b957b08f = $this->env->getExtension("native_profiler");
        $__internal_a8b659d5416d33e4b5b8cd22a943708708487af36d6b17c2c1119153b957b08f->enter($__internal_a8b659d5416d33e4b5b8cd22a943708708487af36d6b17c2c1119153b957b08f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_a8b659d5416d33e4b5b8cd22a943708708487af36d6b17c2c1119153b957b08f->leave($__internal_a8b659d5416d33e4b5b8cd22a943708708487af36d6b17c2c1119153b957b08f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
