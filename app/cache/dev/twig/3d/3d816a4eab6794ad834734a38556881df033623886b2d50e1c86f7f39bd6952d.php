<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_f5e011a90be449de01f34ffc4e74f781ec8e40b8661e2f0ff83281d75b617ec6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eef69d8c06f4274248e08d6d65e44ebc01c0d8f239cff5504abf6002532138c6 = $this->env->getExtension("native_profiler");
        $__internal_eef69d8c06f4274248e08d6d65e44ebc01c0d8f239cff5504abf6002532138c6->enter($__internal_eef69d8c06f4274248e08d6d65e44ebc01c0d8f239cff5504abf6002532138c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_eef69d8c06f4274248e08d6d65e44ebc01c0d8f239cff5504abf6002532138c6->leave($__internal_eef69d8c06f4274248e08d6d65e44ebc01c0d8f239cff5504abf6002532138c6_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
