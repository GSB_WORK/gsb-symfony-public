<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_c54c0fe0c6fbc3def2fd5e67d5e25cdf0a0d3563ecedb15e2596299850a829a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ad88a08f87928790a891aef01d6f49951d64d6ca07106e785ca0118f6d428cad = $this->env->getExtension("native_profiler");
        $__internal_ad88a08f87928790a891aef01d6f49951d64d6ca07106e785ca0118f6d428cad->enter($__internal_ad88a08f87928790a891aef01d6f49951d64d6ca07106e785ca0118f6d428cad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_ad88a08f87928790a891aef01d6f49951d64d6ca07106e785ca0118f6d428cad->leave($__internal_ad88a08f87928790a891aef01d6f49951d64d6ca07106e785ca0118f6d428cad_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
