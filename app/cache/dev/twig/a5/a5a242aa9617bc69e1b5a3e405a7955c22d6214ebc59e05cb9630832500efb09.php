<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_fbd79d0c246ebf187da2a69f7c7c24b5454df29b449d9f398e80ffcd1ad3f84a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e9a9e7072c08a265cdf227248724bbe5032372f975e92fb8ee968f8a5c32cace = $this->env->getExtension("native_profiler");
        $__internal_e9a9e7072c08a265cdf227248724bbe5032372f975e92fb8ee968f8a5c32cace->enter($__internal_e9a9e7072c08a265cdf227248724bbe5032372f975e92fb8ee968f8a5c32cace_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_e9a9e7072c08a265cdf227248724bbe5032372f975e92fb8ee968f8a5c32cace->leave($__internal_e9a9e7072c08a265cdf227248724bbe5032372f975e92fb8ee968f8a5c32cace_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
