<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_22397cf1a25d176d9847adcc0897435406710bd1f128947cab956d5b9d1d332a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_094dab8e7b21d06f33074c3e17c07eaffc86f55ba45733a880e8a7d07c2860d3 = $this->env->getExtension("native_profiler");
        $__internal_094dab8e7b21d06f33074c3e17c07eaffc86f55ba45733a880e8a7d07c2860d3->enter($__internal_094dab8e7b21d06f33074c3e17c07eaffc86f55ba45733a880e8a7d07c2860d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_094dab8e7b21d06f33074c3e17c07eaffc86f55ba45733a880e8a7d07c2860d3->leave($__internal_094dab8e7b21d06f33074c3e17c07eaffc86f55ba45733a880e8a7d07c2860d3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
