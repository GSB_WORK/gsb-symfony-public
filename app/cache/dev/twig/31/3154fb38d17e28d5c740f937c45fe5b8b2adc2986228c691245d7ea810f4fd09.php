<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_eb519ff453d5ec1ab5dc674c52102f01e0905ef5d4c4962b3e5eb196d68bc67e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_59608a8e568a834cff8cce3c9f204c49c6b0ab36e77ca33d6964ab5e47257340 = $this->env->getExtension("native_profiler");
        $__internal_59608a8e568a834cff8cce3c9f204c49c6b0ab36e77ca33d6964ab5e47257340->enter($__internal_59608a8e568a834cff8cce3c9f204c49c6b0ab36e77ca33d6964ab5e47257340_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_59608a8e568a834cff8cce3c9f204c49c6b0ab36e77ca33d6964ab5e47257340->leave($__internal_59608a8e568a834cff8cce3c9f204c49c6b0ab36e77ca33d6964ab5e47257340_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
