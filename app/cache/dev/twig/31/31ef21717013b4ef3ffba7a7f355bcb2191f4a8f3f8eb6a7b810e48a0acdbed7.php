<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_15a0cc99b09b30a4bb76c62a30043dc40817602f3ec5f7b56ef03ab45d068436 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_702dc9aeda4bfc6972311168a779303b75d7efbf70e73208d37d388c5a6e01b2 = $this->env->getExtension("native_profiler");
        $__internal_702dc9aeda4bfc6972311168a779303b75d7efbf70e73208d37d388c5a6e01b2->enter($__internal_702dc9aeda4bfc6972311168a779303b75d7efbf70e73208d37d388c5a6e01b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_702dc9aeda4bfc6972311168a779303b75d7efbf70e73208d37d388c5a6e01b2->leave($__internal_702dc9aeda4bfc6972311168a779303b75d7efbf70e73208d37d388c5a6e01b2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
