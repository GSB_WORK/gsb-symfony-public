<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_05508911baee4fee6e6c283d4172f21184e7097610d6ea8fff0674d8b27efb7e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21d97f9743558b20f38d221f2f51e211328fb15758225d31d2e50f62f0a945ae = $this->env->getExtension("native_profiler");
        $__internal_21d97f9743558b20f38d221f2f51e211328fb15758225d31d2e50f62f0a945ae->enter($__internal_21d97f9743558b20f38d221f2f51e211328fb15758225d31d2e50f62f0a945ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_21d97f9743558b20f38d221f2f51e211328fb15758225d31d2e50f62f0a945ae->leave($__internal_21d97f9743558b20f38d221f2f51e211328fb15758225d31d2e50f62f0a945ae_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
