<?php

/* PgGsbFraisBundle:Default:index.html.twig */
class __TwigTemplate_2d0b4d1a365f6890b311304e3d30f5487755d25c827398aa6b756fa53e37a56a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4affa0d9e212ab01618c063dda160b034277f4944a8bec0609c3817f76232898 = $this->env->getExtension("native_profiler");
        $__internal_4affa0d9e212ab01618c063dda160b034277f4944a8bec0609c3817f76232898->enter($__internal_4affa0d9e212ab01618c063dda160b034277f4944a8bec0609c3817f76232898_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PgGsbFraisBundle:Default:index.html.twig"));

        // line 1
        echo "Hello ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "!
";
        
        $__internal_4affa0d9e212ab01618c063dda160b034277f4944a8bec0609c3817f76232898->leave($__internal_4affa0d9e212ab01618c063dda160b034277f4944a8bec0609c3817f76232898_prof);

    }

    public function getTemplateName()
    {
        return "PgGsbFraisBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* Hello {{ name }}!*/
/* */
