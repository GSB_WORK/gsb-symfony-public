<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_82249c98b5ad77967f5052aa11a89aaeeb29cd26591815450a49caf26dba390f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc0160f57320b62072ce6ef9410b01df5fd7958cbf2709c4d042eadd29a1c453 = $this->env->getExtension("native_profiler");
        $__internal_bc0160f57320b62072ce6ef9410b01df5fd7958cbf2709c4d042eadd29a1c453->enter($__internal_bc0160f57320b62072ce6ef9410b01df5fd7958cbf2709c4d042eadd29a1c453_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_bc0160f57320b62072ce6ef9410b01df5fd7958cbf2709c4d042eadd29a1c453->leave($__internal_bc0160f57320b62072ce6ef9410b01df5fd7958cbf2709c4d042eadd29a1c453_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
