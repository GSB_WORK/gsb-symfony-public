<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_84f04eeb429cd2eacf07be07e7505b667aea53a3e56bb16b8298bbbb75eb1968 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c2edc4b9893a5ef56998baf0462515d6fe8bfdaf84454be665d759b63f8fc109 = $this->env->getExtension("native_profiler");
        $__internal_c2edc4b9893a5ef56998baf0462515d6fe8bfdaf84454be665d759b63f8fc109->enter($__internal_c2edc4b9893a5ef56998baf0462515d6fe8bfdaf84454be665d759b63f8fc109_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_c2edc4b9893a5ef56998baf0462515d6fe8bfdaf84454be665d759b63f8fc109->leave($__internal_c2edc4b9893a5ef56998baf0462515d6fe8bfdaf84454be665d759b63f8fc109_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
