<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_a061ef9f742ebbb913acbaecb86b99072297eb1d2e182e81926a854d072e2901 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_515fcb0b2439199ea1e618d58fa5d8f460db77fe2d78d3275edf6eafdd4d9153 = $this->env->getExtension("native_profiler");
        $__internal_515fcb0b2439199ea1e618d58fa5d8f460db77fe2d78d3275edf6eafdd4d9153->enter($__internal_515fcb0b2439199ea1e618d58fa5d8f460db77fe2d78d3275edf6eafdd4d9153_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_515fcb0b2439199ea1e618d58fa5d8f460db77fe2d78d3275edf6eafdd4d9153->leave($__internal_515fcb0b2439199ea1e618d58fa5d8f460db77fe2d78d3275edf6eafdd4d9153_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
