<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_deccbc5befad7482b0b1e4358c90c14fe7cdc6a882a1c853d79c704c618d06c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5df8b22589f3745a95ce085b336347e62a62550da40195a7e0c47b764bcacb8d = $this->env->getExtension("native_profiler");
        $__internal_5df8b22589f3745a95ce085b336347e62a62550da40195a7e0c47b764bcacb8d->enter($__internal_5df8b22589f3745a95ce085b336347e62a62550da40195a7e0c47b764bcacb8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_5df8b22589f3745a95ce085b336347e62a62550da40195a7e0c47b764bcacb8d->leave($__internal_5df8b22589f3745a95ce085b336347e62a62550da40195a7e0c47b764bcacb8d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
