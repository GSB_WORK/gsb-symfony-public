<?php

/* PgGsbFraisBundle:SaisirFrais:saisirtouslesfrais.html.twig */
class __TwigTemplate_1adbb117ab5969b15bcec4dd1864ba0872f440e415e89a66a9427c3a8c8fdb14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PgGsbFraisBundle::accueil.html.twig", "PgGsbFraisBundle:SaisirFrais:saisirtouslesfrais.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'bloc1' => array($this, 'block_bloc1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PgGsbFraisBundle::accueil.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6a3c923f8bb965dde4c77df180dd9542ed610f5f5c919d9e466e67a41de56343 = $this->env->getExtension("native_profiler");
        $__internal_6a3c923f8bb965dde4c77df180dd9542ed610f5f5c919d9e466e67a41de56343->enter($__internal_6a3c923f8bb965dde4c77df180dd9542ed610f5f5c919d9e466e67a41de56343_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PgGsbFraisBundle:SaisirFrais:saisirtouslesfrais.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6a3c923f8bb965dde4c77df180dd9542ed610f5f5c919d9e466e67a41de56343->leave($__internal_6a3c923f8bb965dde4c77df180dd9542ed610f5f5c919d9e466e67a41de56343_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_ce4c4e7737f48bd3a9d8b76de658d6ac4bc726b3aebcde54c8752895a2b77092 = $this->env->getExtension("native_profiler");
        $__internal_ce4c4e7737f48bd3a9d8b76de658d6ac4bc726b3aebcde54c8752895a2b77092->enter($__internal_ce4c4e7737f48bd3a9d8b76de658d6ac4bc726b3aebcde54c8752895a2b77092_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        $this->displayBlock('bloc1', $context, $blocks);
        
        $__internal_ce4c4e7737f48bd3a9d8b76de658d6ac4bc726b3aebcde54c8752895a2b77092->leave($__internal_ce4c4e7737f48bd3a9d8b76de658d6ac4bc726b3aebcde54c8752895a2b77092_prof);

    }

    public function block_bloc1($context, array $blocks = array())
    {
        $__internal_61a8982d00b7f42ec0a60897b05e4bcfc80caeb6460b0dfd0b230957b8641608 = $this->env->getExtension("native_profiler");
        $__internal_61a8982d00b7f42ec0a60897b05e4bcfc80caeb6460b0dfd0b230957b8641608->enter($__internal_61a8982d00b7f42ec0a60897b05e4bcfc80caeb6460b0dfd0b230957b8641608_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bloc1"));

        // line 4
        echo " ";
        $this->loadTemplate("PgGsbFraisBundle:SaisirFrais:erreurs.html.twig", "PgGsbFraisBundle:SaisirFrais:saisirtouslesfrais.html.twig", 4)->display(array_merge($context, array("leserreurs" => (isset($context["leserreursforfait"]) ? $context["leserreursforfait"] : $this->getContext($context, "leserreursforfait")))));
        // line 5
        $this->loadTemplate("PgGsbFraisBundle:SaisirFrais:saisirfraisforfait.html.twig", "PgGsbFraisBundle:SaisirFrais:saisirtouslesfrais.html.twig", 5)->display(array_merge($context, array("lesfraisforfait" => (isset($context["lesfraisforfait"]) ? $context["lesfraisforfait"] : $this->getContext($context, "lesfraisforfait")), "nummois" =>         // line 6
(isset($context["nummois"]) ? $context["nummois"] : $this->getContext($context, "nummois")), "numannee" => (isset($context["numannee"]) ? $context["numannee"] : $this->getContext($context, "numannee")))));
        // line 7
        echo "
";
        // line 8
        $this->loadTemplate("PgGsbFraisBundle:SaisirFrais:erreurs.html.twig", "PgGsbFraisBundle:SaisirFrais:saisirtouslesfrais.html.twig", 8)->display(array_merge($context, array("leserreurs" => (isset($context["leserreurshorsforfait"]) ? $context["leserreurshorsforfait"] : $this->getContext($context, "leserreurshorsforfait")))));
        // line 9
        $this->loadTemplate("PgGsbFraisBundle:SaisirFrais:saisirfraishorsforfait.html.twig", "PgGsbFraisBundle:SaisirFrais:saisirtouslesfrais.html.twig", 9)->display(array_merge($context, array("lesfraishorsforfait" => (isset($context["lesfraishorsforfait"]) ? $context["lesfraishorsforfait"] : $this->getContext($context, "lesfraishorsforfait")))));
        
        $__internal_61a8982d00b7f42ec0a60897b05e4bcfc80caeb6460b0dfd0b230957b8641608->leave($__internal_61a8982d00b7f42ec0a60897b05e4bcfc80caeb6460b0dfd0b230957b8641608_prof);

    }

    public function getTemplateName()
    {
        return "PgGsbFraisBundle:SaisirFrais:saisirtouslesfrais.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 9,  62 => 8,  59 => 7,  57 => 6,  56 => 5,  53 => 4,  41 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends "PgGsbFraisBundle::accueil.html.twig" %}*/
/* {% block body %}*/
/* {% block bloc1 %}*/
/*  {% include 'PgGsbFraisBundle:SaisirFrais:erreurs.html.twig' with {'leserreurs':leserreursforfait} %}*/
/* {% include 'PgGsbFraisBundle:SaisirFrais:saisirfraisforfait.html.twig' with {'lesfraisforfait':lesfraisforfait,*/
/* 'nummois':nummois,'numannee':numannee} %}*/
/* */
/* {% include 'PgGsbFraisBundle:SaisirFrais:erreurs.html.twig' with {'leserreurs':leserreurshorsforfait} %}*/
/* {% include 'PgGsbFraisBundle:SaisirFrais:saisirfraishorsforfait.html.twig' with {'lesfraishorsforfait':lesfraishorsforfait} %}*/
/* {% endblock %}*/
/* {% endblock %}*/
/* */
/* */
