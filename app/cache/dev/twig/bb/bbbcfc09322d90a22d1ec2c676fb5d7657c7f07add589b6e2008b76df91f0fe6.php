<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_d980e287140acebceb3d805f83261cad53e6fea1a92b9e1a165c05f185fb0b91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_549e9d9efebb4c83152dd1544c31f7bf3787dd2471e39e887b965812e398541d = $this->env->getExtension("native_profiler");
        $__internal_549e9d9efebb4c83152dd1544c31f7bf3787dd2471e39e887b965812e398541d->enter($__internal_549e9d9efebb4c83152dd1544c31f7bf3787dd2471e39e887b965812e398541d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_549e9d9efebb4c83152dd1544c31f7bf3787dd2471e39e887b965812e398541d->leave($__internal_549e9d9efebb4c83152dd1544c31f7bf3787dd2471e39e887b965812e398541d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
