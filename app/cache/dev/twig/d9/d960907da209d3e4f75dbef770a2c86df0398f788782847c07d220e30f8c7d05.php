<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_3473da5abb44c01f375f4f18b7ab88853a96f3580057a57d821ac5b740990b6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e59cdb8aff97d1ac129717d462d0307510665503fc740cfcd306a8841e472ad1 = $this->env->getExtension("native_profiler");
        $__internal_e59cdb8aff97d1ac129717d462d0307510665503fc740cfcd306a8841e472ad1->enter($__internal_e59cdb8aff97d1ac129717d462d0307510665503fc740cfcd306a8841e472ad1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e59cdb8aff97d1ac129717d462d0307510665503fc740cfcd306a8841e472ad1->leave($__internal_e59cdb8aff97d1ac129717d462d0307510665503fc740cfcd306a8841e472ad1_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_f8c133cf5f664c5b3e7b5451ebe7ed57c4bf57882959a69a6bbd07d5dcf53cba = $this->env->getExtension("native_profiler");
        $__internal_f8c133cf5f664c5b3e7b5451ebe7ed57c4bf57882959a69a6bbd07d5dcf53cba->enter($__internal_f8c133cf5f664c5b3e7b5451ebe7ed57c4bf57882959a69a6bbd07d5dcf53cba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_f8c133cf5f664c5b3e7b5451ebe7ed57c4bf57882959a69a6bbd07d5dcf53cba->leave($__internal_f8c133cf5f664c5b3e7b5451ebe7ed57c4bf57882959a69a6bbd07d5dcf53cba_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_22838fcd335822b1c0dd1ef49e8578fe93e97e6dc1fa167d86393e4af7543083 = $this->env->getExtension("native_profiler");
        $__internal_22838fcd335822b1c0dd1ef49e8578fe93e97e6dc1fa167d86393e4af7543083->enter($__internal_22838fcd335822b1c0dd1ef49e8578fe93e97e6dc1fa167d86393e4af7543083_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_22838fcd335822b1c0dd1ef49e8578fe93e97e6dc1fa167d86393e4af7543083->leave($__internal_22838fcd335822b1c0dd1ef49e8578fe93e97e6dc1fa167d86393e4af7543083_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_34eb2a3219ca91426c67a125159a33b9caa29d461f0f265db105fad034ab32b5 = $this->env->getExtension("native_profiler");
        $__internal_34eb2a3219ca91426c67a125159a33b9caa29d461f0f265db105fad034ab32b5->enter($__internal_34eb2a3219ca91426c67a125159a33b9caa29d461f0f265db105fad034ab32b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_34eb2a3219ca91426c67a125159a33b9caa29d461f0f265db105fad034ab32b5->leave($__internal_34eb2a3219ca91426c67a125159a33b9caa29d461f0f265db105fad034ab32b5_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
