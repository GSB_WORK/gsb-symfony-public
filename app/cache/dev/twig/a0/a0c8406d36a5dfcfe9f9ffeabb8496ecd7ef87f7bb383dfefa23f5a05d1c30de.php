<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_40718cd1de806b4c88c93affb54f0afaa8e8e3da3cf545d55f5ab85ebe6ee2af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8645fc0bd038565f55162616079f68e469f70f4f502ed544b144a6843a87114d = $this->env->getExtension("native_profiler");
        $__internal_8645fc0bd038565f55162616079f68e469f70f4f502ed544b144a6843a87114d->enter($__internal_8645fc0bd038565f55162616079f68e469f70f4f502ed544b144a6843a87114d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_8645fc0bd038565f55162616079f68e469f70f4f502ed544b144a6843a87114d->leave($__internal_8645fc0bd038565f55162616079f68e469f70f4f502ed544b144a6843a87114d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
