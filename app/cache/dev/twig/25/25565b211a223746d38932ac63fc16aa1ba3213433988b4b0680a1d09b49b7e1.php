<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_e9fbdc93bab78574d6a482ee0050d7c06e2f99d749049a47c9a2139903f83b12 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1062ed37daa9c0949630b0bc18256a142faa1557c1dd51913d08dbba96e009fa = $this->env->getExtension("native_profiler");
        $__internal_1062ed37daa9c0949630b0bc18256a142faa1557c1dd51913d08dbba96e009fa->enter($__internal_1062ed37daa9c0949630b0bc18256a142faa1557c1dd51913d08dbba96e009fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_1062ed37daa9c0949630b0bc18256a142faa1557c1dd51913d08dbba96e009fa->leave($__internal_1062ed37daa9c0949630b0bc18256a142faa1557c1dd51913d08dbba96e009fa_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
