<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_1ea5133b9649bbcc9aabed4a6659a7d81343cd8598a5a6e436ecd0925751abf9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ca00c128e93f53e49addefa05fc2faf507bd4f9af9dedf3a796b040028bd523 = $this->env->getExtension("native_profiler");
        $__internal_2ca00c128e93f53e49addefa05fc2faf507bd4f9af9dedf3a796b040028bd523->enter($__internal_2ca00c128e93f53e49addefa05fc2faf507bd4f9af9dedf3a796b040028bd523_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_2ca00c128e93f53e49addefa05fc2faf507bd4f9af9dedf3a796b040028bd523->leave($__internal_2ca00c128e93f53e49addefa05fc2faf507bd4f9af9dedf3a796b040028bd523_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->widget($form) ?>*/
/* */
