<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_e310eea86c3420c530410ca43d2c6ce5c17c41e0954921c881e1c433ffc05b7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_91e8ba10f57e37a1e10cd64e950757abda3397e9f0d8da215efe6cf17fd6d57f = $this->env->getExtension("native_profiler");
        $__internal_91e8ba10f57e37a1e10cd64e950757abda3397e9f0d8da215efe6cf17fd6d57f->enter($__internal_91e8ba10f57e37a1e10cd64e950757abda3397e9f0d8da215efe6cf17fd6d57f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_91e8ba10f57e37a1e10cd64e950757abda3397e9f0d8da215efe6cf17fd6d57f->leave($__internal_91e8ba10f57e37a1e10cd64e950757abda3397e9f0d8da215efe6cf17fd6d57f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
