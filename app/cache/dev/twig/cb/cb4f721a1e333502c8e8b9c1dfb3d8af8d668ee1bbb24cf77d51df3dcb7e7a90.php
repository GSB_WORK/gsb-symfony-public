<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_0bf3f808dd41adbcf9c316deff51ce8f41c983d060b67821de204b5083230502 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0b956d35784a6bfb874466271187036c48eb9c242acd132e584ed30b4c5285b = $this->env->getExtension("native_profiler");
        $__internal_f0b956d35784a6bfb874466271187036c48eb9c242acd132e584ed30b4c5285b->enter($__internal_f0b956d35784a6bfb874466271187036c48eb9c242acd132e584ed30b4c5285b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_f0b956d35784a6bfb874466271187036c48eb9c242acd132e584ed30b4c5285b->leave($__internal_f0b956d35784a6bfb874466271187036c48eb9c242acd132e584ed30b4c5285b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
