<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_200b90466be8b90397098892b5efcffe9a0b63cf06f3653d613ade54909f8044 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca24cf84bdabd06ef24e726c915e4c9c97d9511923f3f1cf25274f878ca548fe = $this->env->getExtension("native_profiler");
        $__internal_ca24cf84bdabd06ef24e726c915e4c9c97d9511923f3f1cf25274f878ca548fe->enter($__internal_ca24cf84bdabd06ef24e726c915e4c9c97d9511923f3f1cf25274f878ca548fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_ca24cf84bdabd06ef24e726c915e4c9c97d9511923f3f1cf25274f878ca548fe->leave($__internal_ca24cf84bdabd06ef24e726c915e4c9c97d9511923f3f1cf25274f878ca548fe_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
