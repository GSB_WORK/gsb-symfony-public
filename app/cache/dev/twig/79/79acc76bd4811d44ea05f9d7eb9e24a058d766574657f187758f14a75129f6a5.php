<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_370a8c9a06e53bb02f2325adce484397c77dbb4cbaeec3f933cb7df9bb28e8ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_323669db4456240325c387ee89b5f8a13cfe6f1d9c7d1b5e8831c1ba29c15efd = $this->env->getExtension("native_profiler");
        $__internal_323669db4456240325c387ee89b5f8a13cfe6f1d9c7d1b5e8831c1ba29c15efd->enter($__internal_323669db4456240325c387ee89b5f8a13cfe6f1d9c7d1b5e8831c1ba29c15efd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_323669db4456240325c387ee89b5f8a13cfe6f1d9c7d1b5e8831c1ba29c15efd->leave($__internal_323669db4456240325c387ee89b5f8a13cfe6f1d9c7d1b5e8831c1ba29c15efd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
