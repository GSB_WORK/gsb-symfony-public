<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_5be03a0b83df3bf25eddd6ee500363b1cdc3de7e757a9adca012959eb75499fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a75f501a9eb2c8853b551cc435f38c8db94d171e072dd20822dec7b76415b95 = $this->env->getExtension("native_profiler");
        $__internal_0a75f501a9eb2c8853b551cc435f38c8db94d171e072dd20822dec7b76415b95->enter($__internal_0a75f501a9eb2c8853b551cc435f38c8db94d171e072dd20822dec7b76415b95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_0a75f501a9eb2c8853b551cc435f38c8db94d171e072dd20822dec7b76415b95->leave($__internal_0a75f501a9eb2c8853b551cc435f38c8db94d171e072dd20822dec7b76415b95_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_47fd790140ef47a9bf2b8938ad0506885c3c86ccb79bcccfaa53bf9852fe0117 = $this->env->getExtension("native_profiler");
        $__internal_47fd790140ef47a9bf2b8938ad0506885c3c86ccb79bcccfaa53bf9852fe0117->enter($__internal_47fd790140ef47a9bf2b8938ad0506885c3c86ccb79bcccfaa53bf9852fe0117_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_47fd790140ef47a9bf2b8938ad0506885c3c86ccb79bcccfaa53bf9852fe0117->leave($__internal_47fd790140ef47a9bf2b8938ad0506885c3c86ccb79bcccfaa53bf9852fe0117_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
