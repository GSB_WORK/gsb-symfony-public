<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_aff7a47d4e655fac7940485417133b74348caaa80ed4bd7bddce33b013d33452 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5e07e1e4f1a243c918c83e293079e7e14da5eb8678fdfd693361a8ba935f455 = $this->env->getExtension("native_profiler");
        $__internal_e5e07e1e4f1a243c918c83e293079e7e14da5eb8678fdfd693361a8ba935f455->enter($__internal_e5e07e1e4f1a243c918c83e293079e7e14da5eb8678fdfd693361a8ba935f455_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_e5e07e1e4f1a243c918c83e293079e7e14da5eb8678fdfd693361a8ba935f455->leave($__internal_e5e07e1e4f1a243c918c83e293079e7e14da5eb8678fdfd693361a8ba935f455_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
