<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_30d91c9b10f4fe032d20528b52fa1168f5b487c2572de40a1fc0141c37238d39 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c270e1647782c142760a549d364da0a335849e717e40f5e655b32eb1a1ce766 = $this->env->getExtension("native_profiler");
        $__internal_3c270e1647782c142760a549d364da0a335849e717e40f5e655b32eb1a1ce766->enter($__internal_3c270e1647782c142760a549d364da0a335849e717e40f5e655b32eb1a1ce766_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_3c270e1647782c142760a549d364da0a335849e717e40f5e655b32eb1a1ce766->leave($__internal_3c270e1647782c142760a549d364da0a335849e717e40f5e655b32eb1a1ce766_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!$label) { $label = isset($label_format)*/
/*     ? strtr($label_format, array('%name%' => $name, '%id%' => $id))*/
/*     : $view['form']->humanize($name); } ?>*/
/* <button type="<?php echo isset($type) ? $view->escape($type) : 'button' ?>" <?php echo $view['form']->block($form, 'button_attributes') ?>><?php echo $view->escape(false !== $translation_domain ? $view['translator']->trans($label, array(), $translation_domain) : $label) ?></button>*/
/* */
