<?php

/* PgGsbFraisBundle:Home:connexion.html.twig */
class __TwigTemplate_51c811d18969401bb8efcc3cb93fea5c5a70023c78dd7b57e85003302d5e7d5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PgGsbFraisBundle::layout.html.twig", "PgGsbFraisBundle:Home:connexion.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'bloc1' => array($this, 'block_bloc1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PgGsbFraisBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d3e50abbdfe637b1d3a0898ea561cae8ed6a5f38c9c194ce671f5d25757a54ba = $this->env->getExtension("native_profiler");
        $__internal_d3e50abbdfe637b1d3a0898ea561cae8ed6a5f38c9c194ce671f5d25757a54ba->enter($__internal_d3e50abbdfe637b1d3a0898ea561cae8ed6a5f38c9c194ce671f5d25757a54ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PgGsbFraisBundle:Home:connexion.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d3e50abbdfe637b1d3a0898ea561cae8ed6a5f38c9c194ce671f5d25757a54ba->leave($__internal_d3e50abbdfe637b1d3a0898ea561cae8ed6a5f38c9c194ce671f5d25757a54ba_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_e4538ec74e561eeb1c7fd45e0b826e5221870416fe44ae530a8f7fc4628c6596 = $this->env->getExtension("native_profiler");
        $__internal_e4538ec74e561eeb1c7fd45e0b826e5221870416fe44ae530a8f7fc4628c6596->enter($__internal_e4538ec74e561eeb1c7fd45e0b826e5221870416fe44ae530a8f7fc4628c6596_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo " ";
        $this->displayBlock('bloc1', $context, $blocks);
        // line 23
        echo "    
    ";
        
        $__internal_e4538ec74e561eeb1c7fd45e0b826e5221870416fe44ae530a8f7fc4628c6596->leave($__internal_e4538ec74e561eeb1c7fd45e0b826e5221870416fe44ae530a8f7fc4628c6596_prof);

    }

    // line 3
    public function block_bloc1($context, array $blocks = array())
    {
        $__internal_c9da3bd072b924f0226c521ee496dcfac25ae7f9d7bb9f276aa16f22a720a279 = $this->env->getExtension("native_profiler");
        $__internal_c9da3bd072b924f0226c521ee496dcfac25ae7f9d7bb9f276aa16f22a720a279->enter($__internal_c9da3bd072b924f0226c521ee496dcfac25ae7f9d7bb9f276aa16f22a720a279_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bloc1"));

        echo " 
 <div id=\"contenu\">
 <div class=\"row\">
      <div class=\"col m6 offset-m3\">
<form method=\"POST\" action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_validerconnexion");
        echo "\" id=\"form-login\">
        <p>
          <label for=\"nom\">Login :</label>
           <input placeholder=\"Identifiant...\" id=\"login\" type=\"text\" name=\"login\"  size=\"30\" maxlength=\"45\">
        </p>
\t<p>
            <label for=\"mdp\">Mot de passe :</label>
            <input placeholder=\"Mot de passe...\" id=\"mdp\"  type=\"password\"  name=\"mdp\" size=\"30\" maxlength=\"45\">
        </p>
         <input class=\"waves-effect waves-light btn blue\" type=\"submit\" value=\"Valider\" name=\"valider\">
         <input class=\"waves-effect waves-light btn blue\" type=\"reset\" value=\"Annuler\" name=\"annuler\"> 
      </p>
</form>
</div>
</div>
</div>
    ";
        
        $__internal_c9da3bd072b924f0226c521ee496dcfac25ae7f9d7bb9f276aa16f22a720a279->leave($__internal_c9da3bd072b924f0226c521ee496dcfac25ae7f9d7bb9f276aa16f22a720a279_prof);

    }

    public function getTemplateName()
    {
        return "PgGsbFraisBundle:Home:connexion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 7,  52 => 3,  44 => 23,  41 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends "PgGsbFraisBundle::layout.html.twig" %}*/
/* {% block body %}*/
/*  {% block bloc1 %} */
/*  <div id="contenu">*/
/*  <div class="row">*/
/*       <div class="col m6 offset-m3">*/
/* <form method="POST" action="{{path('pg_gsb_frais_validerconnexion')}}" id="form-login">*/
/*         <p>*/
/*           <label for="nom">Login :</label>*/
/*            <input placeholder="Identifiant..." id="login" type="text" name="login"  size="30" maxlength="45">*/
/*         </p>*/
/* 	<p>*/
/*             <label for="mdp">Mot de passe :</label>*/
/*             <input placeholder="Mot de passe..." id="mdp"  type="password"  name="mdp" size="30" maxlength="45">*/
/*         </p>*/
/*          <input class="waves-effect waves-light btn blue" type="submit" value="Valider" name="valider">*/
/*          <input class="waves-effect waves-light btn blue" type="reset" value="Annuler" name="annuler"> */
/*       </p>*/
/* </form>*/
/* </div>*/
/* </div>*/
/* </div>*/
/*     {% endblock %}    */
/*     {% endblock %}*/
/* */
/*      */
