<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_6603ea9afda1d7b04d42f6a266ebd762caae4058f22acbda309faf5f6468811d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_327168dd69ed7e8911b09a9118c4e34929aec6bf68b3c3dc57df311b36581e22 = $this->env->getExtension("native_profiler");
        $__internal_327168dd69ed7e8911b09a9118c4e34929aec6bf68b3c3dc57df311b36581e22->enter($__internal_327168dd69ed7e8911b09a9118c4e34929aec6bf68b3c3dc57df311b36581e22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_327168dd69ed7e8911b09a9118c4e34929aec6bf68b3c3dc57df311b36581e22->leave($__internal_327168dd69ed7e8911b09a9118c4e34929aec6bf68b3c3dc57df311b36581e22_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
