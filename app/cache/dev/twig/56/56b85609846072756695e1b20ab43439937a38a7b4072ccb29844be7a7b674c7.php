<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_aebd63dcb4cfd32cb03d2c70291a7558bc4567f79d9b175a0f410b338cbb4fda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_15002d05b8e327166249c36f59b7e7591edfac048959a4744bf7e09f90cc0b56 = $this->env->getExtension("native_profiler");
        $__internal_15002d05b8e327166249c36f59b7e7591edfac048959a4744bf7e09f90cc0b56->enter($__internal_15002d05b8e327166249c36f59b7e7591edfac048959a4744bf7e09f90cc0b56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_15002d05b8e327166249c36f59b7e7591edfac048959a4744bf7e09f90cc0b56->leave($__internal_15002d05b8e327166249c36f59b7e7591edfac048959a4744bf7e09f90cc0b56_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
