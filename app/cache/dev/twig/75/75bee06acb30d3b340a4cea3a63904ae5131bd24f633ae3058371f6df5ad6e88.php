<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_2358857d4833a8f5af68618b08f7d3734420331c627352dd3412e7e7e2d173c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4ded5cc970fa57076a69f63a0b186367ebbc5535784bf65f8a83650e5867ac7 = $this->env->getExtension("native_profiler");
        $__internal_b4ded5cc970fa57076a69f63a0b186367ebbc5535784bf65f8a83650e5867ac7->enter($__internal_b4ded5cc970fa57076a69f63a0b186367ebbc5535784bf65f8a83650e5867ac7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_b4ded5cc970fa57076a69f63a0b186367ebbc5535784bf65f8a83650e5867ac7->leave($__internal_b4ded5cc970fa57076a69f63a0b186367ebbc5535784bf65f8a83650e5867ac7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'password')) ?>*/
/* */
