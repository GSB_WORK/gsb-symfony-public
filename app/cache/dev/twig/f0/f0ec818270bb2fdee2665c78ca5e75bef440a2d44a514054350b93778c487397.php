<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_4621022ae73fa74d1dfdc7ed1b3291622855eaf2332c9770a18f8f9363f4725a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3babeb75eecf9097b529beff8cf2aba8ab0349d10e5be26cf8d500eeb00a6031 = $this->env->getExtension("native_profiler");
        $__internal_3babeb75eecf9097b529beff8cf2aba8ab0349d10e5be26cf8d500eeb00a6031->enter($__internal_3babeb75eecf9097b529beff8cf2aba8ab0349d10e5be26cf8d500eeb00a6031_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_3babeb75eecf9097b529beff8cf2aba8ab0349d10e5be26cf8d500eeb00a6031->leave($__internal_3babeb75eecf9097b529beff8cf2aba8ab0349d10e5be26cf8d500eeb00a6031_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="checkbox"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     <?php if (strlen($value) > 0): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?>*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
