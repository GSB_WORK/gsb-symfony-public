<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_1868ef49fc82eb86ae91fe89de6661ac142a42cfde08d38c32313476d458addb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8b40404947c75b43a027de8415b4ee93ab1eb8e01b652d805dba9121788135e4 = $this->env->getExtension("native_profiler");
        $__internal_8b40404947c75b43a027de8415b4ee93ab1eb8e01b652d805dba9121788135e4->enter($__internal_8b40404947c75b43a027de8415b4ee93ab1eb8e01b652d805dba9121788135e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_8b40404947c75b43a027de8415b4ee93ab1eb8e01b652d805dba9121788135e4->leave($__internal_8b40404947c75b43a027de8415b4ee93ab1eb8e01b652d805dba9121788135e4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
