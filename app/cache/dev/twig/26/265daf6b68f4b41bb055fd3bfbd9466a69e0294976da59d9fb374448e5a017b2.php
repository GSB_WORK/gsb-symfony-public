<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_f461141c16b6a59a602fd558f5be2d430244853c67710ce79dc06fc9fec971a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_687c3c86fa21e153a0d7cf8165671111085d4f3a6d45eac6f9e1b0ed1da751bd = $this->env->getExtension("native_profiler");
        $__internal_687c3c86fa21e153a0d7cf8165671111085d4f3a6d45eac6f9e1b0ed1da751bd->enter($__internal_687c3c86fa21e153a0d7cf8165671111085d4f3a6d45eac6f9e1b0ed1da751bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_687c3c86fa21e153a0d7cf8165671111085d4f3a6d45eac6f9e1b0ed1da751bd->leave($__internal_687c3c86fa21e153a0d7cf8165671111085d4f3a6d45eac6f9e1b0ed1da751bd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
