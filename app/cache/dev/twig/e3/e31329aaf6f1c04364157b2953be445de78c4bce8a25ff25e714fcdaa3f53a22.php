<?php

/* PgGsbFraisBundle:Tablette:affectertablette.html.twig */
class __TwigTemplate_c3af8ec0b34fbec7d3b535eb230204fd03e0ff3a82453dcf52b0cde26d9fd977 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PgGsbFraisBundle::accueil.html.twig", "PgGsbFraisBundle:Tablette:affectertablette.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'block1' => array($this, 'block_block1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PgGsbFraisBundle::accueil.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e4c340dfff46a5c7f3982846b33e38ea44bf8373e3ceac53171b7b452e488d2 = $this->env->getExtension("native_profiler");
        $__internal_2e4c340dfff46a5c7f3982846b33e38ea44bf8373e3ceac53171b7b452e488d2->enter($__internal_2e4c340dfff46a5c7f3982846b33e38ea44bf8373e3ceac53171b7b452e488d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PgGsbFraisBundle:Tablette:affectertablette.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2e4c340dfff46a5c7f3982846b33e38ea44bf8373e3ceac53171b7b452e488d2->leave($__internal_2e4c340dfff46a5c7f3982846b33e38ea44bf8373e3ceac53171b7b452e488d2_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_8906bb6920d094a154290226d49a506ee2ee5f6ea20502bdfafd3ec53d92d301 = $this->env->getExtension("native_profiler");
        $__internal_8906bb6920d094a154290226d49a506ee2ee5f6ea20502bdfafd3ec53d92d301->enter($__internal_8906bb6920d094a154290226d49a506ee2ee5f6ea20502bdfafd3ec53d92d301_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<h1>Affecter une tablette à un utilisteur</h1>

<div id=\"contenu\">
      <form action=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_saisirfrais");
        echo "\" method=\"POST\">
       <select name =\"user\" form=\"add-tablette\">
       ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")));
        foreach ($context['_seq'] as $context["_key"] => $context["users"]) {
            // line 9
            echo "       <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["users"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["users"], "nom", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["users"], "prenom", array()), "html", null, true);
            echo "</option>
       ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['users'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "        </select>

        <select name=\"tablette\">
        ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tablette"]) ? $context["tablette"] : $this->getContext($context, "tablette")));
        foreach ($context['_seq'] as $context["_key"] => $context["tablettes"]) {
            // line 15
            echo "        ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tablettes"], "id", array()), "html", null, true);
            echo "

            <option value=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["tablettes"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tablettes"], "idTab", array()), "html", null, true);
            echo "</option> 

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tablettes'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "        </select> 


          <div class=\"piedForm\">
          <p>
            <input class=\"btn blue\" type=\"submit\" value=\"Valider\" size=\"20\" />

          </p> 
          </div>
      </form>
</div>

";
        // line 32
        $this->displayBlock('block1', $context, $blocks);
        
        $__internal_8906bb6920d094a154290226d49a506ee2ee5f6ea20502bdfafd3ec53d92d301->leave($__internal_8906bb6920d094a154290226d49a506ee2ee5f6ea20502bdfafd3ec53d92d301_prof);

    }

    public function block_block1($context, array $blocks = array())
    {
        $__internal_9f7fe627022c3cf87614682a9247568dc421820a82b91cbced6a74d6137dcd73 = $this->env->getExtension("native_profiler");
        $__internal_9f7fe627022c3cf87614682a9247568dc421820a82b91cbced6a74d6137dcd73->enter($__internal_9f7fe627022c3cf87614682a9247568dc421820a82b91cbced6a74d6137dcd73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block1"));

        // line 33
        echo "
";
        
        $__internal_9f7fe627022c3cf87614682a9247568dc421820a82b91cbced6a74d6137dcd73->leave($__internal_9f7fe627022c3cf87614682a9247568dc421820a82b91cbced6a74d6137dcd73_prof);

    }

    public function getTemplateName()
    {
        return "PgGsbFraisBundle:Tablette:affectertablette.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 33,  108 => 32,  94 => 20,  83 => 17,  77 => 15,  73 => 14,  68 => 11,  55 => 9,  51 => 8,  46 => 6,  41 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends "PgGsbFraisBundle::accueil.html.twig" %}*/
/* {% block body %}*/
/* <h1>Affecter une tablette à un utilisteur</h1>*/
/* */
/* <div id="contenu">*/
/*       <form action="{{path('pg_gsb_frais_saisirfrais')}}" method="POST">*/
/*        <select name ="user" form="add-tablette">*/
/*        {% for users in user %}*/
/*        <option value="{{users.id}}">{{users.nom}} {{users.prenom}}</option>*/
/*        {% endfor %}*/
/*         </select>*/
/* */
/*         <select name="tablette">*/
/*         {% for tablettes in tablette %}*/
/*         {{tablettes.id}}*/
/* */
/*             <option value="{{tablettes.id}}">{{tablettes.idTab}}</option> */
/* */
/*         {% endfor %}*/
/*         </select> */
/* */
/* */
/*           <div class="piedForm">*/
/*           <p>*/
/*             <input class="btn blue" type="submit" value="Valider" size="20" />*/
/* */
/*           </p> */
/*           </div>*/
/*       </form>*/
/* </div>*/
/* */
/* {% block block1 %}*/
/* */
/* {% endblock %}*/
/* {% endblock %}*/
