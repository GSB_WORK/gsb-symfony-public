<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_dbe246029bc83a6d2b977595c89f03284b5360264e7472ffc67b5ea57a5061c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41dcdaef0979ed63f8592dc57835e239314fea691a2aaf89b35ac92e6495e5aa = $this->env->getExtension("native_profiler");
        $__internal_41dcdaef0979ed63f8592dc57835e239314fea691a2aaf89b35ac92e6495e5aa->enter($__internal_41dcdaef0979ed63f8592dc57835e239314fea691a2aaf89b35ac92e6495e5aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_41dcdaef0979ed63f8592dc57835e239314fea691a2aaf89b35ac92e6495e5aa->leave($__internal_41dcdaef0979ed63f8592dc57835e239314fea691a2aaf89b35ac92e6495e5aa_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
