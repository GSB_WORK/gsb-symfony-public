<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_aed2ee549d9724c41455460dc636ecb8b53dc627676b4584d0aaf30abc5d894f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7dba518e63d23aa5e9c1e0bb67808100405a0ca9c408d9fe79790fa325046177 = $this->env->getExtension("native_profiler");
        $__internal_7dba518e63d23aa5e9c1e0bb67808100405a0ca9c408d9fe79790fa325046177->enter($__internal_7dba518e63d23aa5e9c1e0bb67808100405a0ca9c408d9fe79790fa325046177_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_7dba518e63d23aa5e9c1e0bb67808100405a0ca9c408d9fe79790fa325046177->leave($__internal_7dba518e63d23aa5e9c1e0bb67808100405a0ca9c408d9fe79790fa325046177_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
