<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_640da302b4bbe3928e76e169ef117fddb0dd56aa67246847cda9b321dae54a92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f3cecd048e9dc6328c7a1dbb2c38c580e7b1794e42e274520670dc255f54dd0 = $this->env->getExtension("native_profiler");
        $__internal_6f3cecd048e9dc6328c7a1dbb2c38c580e7b1794e42e274520670dc255f54dd0->enter($__internal_6f3cecd048e9dc6328c7a1dbb2c38c580e7b1794e42e274520670dc255f54dd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f3cecd048e9dc6328c7a1dbb2c38c580e7b1794e42e274520670dc255f54dd0->leave($__internal_6f3cecd048e9dc6328c7a1dbb2c38c580e7b1794e42e274520670dc255f54dd0_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6d0746b1fc326bb997b994fd883f5dcf17a37f977a520204114ec0f75322393c = $this->env->getExtension("native_profiler");
        $__internal_6d0746b1fc326bb997b994fd883f5dcf17a37f977a520204114ec0f75322393c->enter($__internal_6d0746b1fc326bb997b994fd883f5dcf17a37f977a520204114ec0f75322393c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_6d0746b1fc326bb997b994fd883f5dcf17a37f977a520204114ec0f75322393c->leave($__internal_6d0746b1fc326bb997b994fd883f5dcf17a37f977a520204114ec0f75322393c_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_5220367ee5f79f840a8b379b471f0913df85a4852733755e76625ac159bef7a3 = $this->env->getExtension("native_profiler");
        $__internal_5220367ee5f79f840a8b379b471f0913df85a4852733755e76625ac159bef7a3->enter($__internal_5220367ee5f79f840a8b379b471f0913df85a4852733755e76625ac159bef7a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_5220367ee5f79f840a8b379b471f0913df85a4852733755e76625ac159bef7a3->leave($__internal_5220367ee5f79f840a8b379b471f0913df85a4852733755e76625ac159bef7a3_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_29b6d20ac82a64cc37b12bea7459e3dbd4d5b4705fa17b2b0745aa41000aece0 = $this->env->getExtension("native_profiler");
        $__internal_29b6d20ac82a64cc37b12bea7459e3dbd4d5b4705fa17b2b0745aa41000aece0->enter($__internal_29b6d20ac82a64cc37b12bea7459e3dbd4d5b4705fa17b2b0745aa41000aece0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_29b6d20ac82a64cc37b12bea7459e3dbd4d5b4705fa17b2b0745aa41000aece0->leave($__internal_29b6d20ac82a64cc37b12bea7459e3dbd4d5b4705fa17b2b0745aa41000aece0_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
