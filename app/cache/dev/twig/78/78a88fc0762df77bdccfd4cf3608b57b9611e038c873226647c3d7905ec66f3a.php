<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_8c7f40f2e642118e6a4fb649afae65547af9cbac4a59c4087049d419d3a57310 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eece3610d28cb14a580fd0e3fab094a8d07d52a23d7a96da9e76b2bd0615949d = $this->env->getExtension("native_profiler");
        $__internal_eece3610d28cb14a580fd0e3fab094a8d07d52a23d7a96da9e76b2bd0615949d->enter($__internal_eece3610d28cb14a580fd0e3fab094a8d07d52a23d7a96da9e76b2bd0615949d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_eece3610d28cb14a580fd0e3fab094a8d07d52a23d7a96da9e76b2bd0615949d->leave($__internal_eece3610d28cb14a580fd0e3fab094a8d07d52a23d7a96da9e76b2bd0615949d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
