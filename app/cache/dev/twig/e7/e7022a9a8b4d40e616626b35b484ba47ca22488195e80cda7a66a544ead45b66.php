<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_de06ae6766740d98f7bb14a09c70c2264c3bedadeb85e0f36b11398d0683d648 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fe29e15a7fadd789ff512dd5efd923eee3a9001a44b89767fe23d54bd0aedcf7 = $this->env->getExtension("native_profiler");
        $__internal_fe29e15a7fadd789ff512dd5efd923eee3a9001a44b89767fe23d54bd0aedcf7->enter($__internal_fe29e15a7fadd789ff512dd5efd923eee3a9001a44b89767fe23d54bd0aedcf7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_fe29e15a7fadd789ff512dd5efd923eee3a9001a44b89767fe23d54bd0aedcf7->leave($__internal_fe29e15a7fadd789ff512dd5efd923eee3a9001a44b89767fe23d54bd0aedcf7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
