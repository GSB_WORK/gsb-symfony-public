<?php

/* PgGsbFraisBundle:Tablette:creertablette.html.twig */
class __TwigTemplate_7fcdd866046086ab183daf4f29a760198d7be947cf4dc2c91189b73cf51b2be9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PgGsbFraisBundle::accueil.html.twig", "PgGsbFraisBundle:Tablette:creertablette.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PgGsbFraisBundle::accueil.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3d8afcdff266eab2609b5be3057ca047b386e3b82f3f01c14f27f8c656008fd = $this->env->getExtension("native_profiler");
        $__internal_b3d8afcdff266eab2609b5be3057ca047b386e3b82f3f01c14f27f8c656008fd->enter($__internal_b3d8afcdff266eab2609b5be3057ca047b386e3b82f3f01c14f27f8c656008fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PgGsbFraisBundle:Tablette:creertablette.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b3d8afcdff266eab2609b5be3057ca047b386e3b82f3f01c14f27f8c656008fd->leave($__internal_b3d8afcdff266eab2609b5be3057ca047b386e3b82f3f01c14f27f8c656008fd_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_283acc1fe3416ef7bcc5499d493f39806c8a66fd0f981a1b7695f045a9775764 = $this->env->getExtension("native_profiler");
        $__internal_283acc1fe3416ef7bcc5499d493f39806c8a66fd0f981a1b7695f045a9775764->enter($__internal_283acc1fe3416ef7bcc5499d493f39806c8a66fd0f981a1b7695f045a9775764_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<div id=\"container\">
<div class=\"row\">
        ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "info"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 6
            echo "        <div class=\"card-panel light-green white-text\"><i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>
";
            // line 7
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</div>
        
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "      <h2>Ajouter une tablette</h2>
      <form action=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_creertablette");
        echo "\" method=\"POST\" id=\"add-tablette\">
          
          <div class=\"input-field col m3\">
          <select id=\"os-select\" name =\"os\" form=\"add-tablette\">
          <option value=\"Android\">Android</option>
          <option value=\"OSX\">OSX</option>
          </select>
          </div>
<div class=\"input-field col m3\">
          <input type=\"text\" name =\"memoireinterne\" placeholder=\"Mémoire interne\"/>
          </div>
          <div class=\"input-field col m3\">
          <input type =\"text\" id=\"memoire-ext\" name =\"memoireext\" placeholder=\"Mémoire externe\"/>
          </div>
          <div class=\"input-field col m3\">
          
            <input class=\"btn blue waves-effect\" type=\"submit\" value=\"Valider\" size=\"20\" />
          </div>
          </div>
          </div>
      </form>
</div>
";
        
        $__internal_283acc1fe3416ef7bcc5499d493f39806c8a66fd0f981a1b7695f045a9775764->leave($__internal_283acc1fe3416ef7bcc5499d493f39806c8a66fd0f981a1b7695f045a9775764_prof);

    }

    public function getTemplateName()
    {
        return "PgGsbFraisBundle:Tablette:creertablette.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 11,  60 => 10,  51 => 7,  48 => 6,  44 => 5,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "PgGsbFraisBundle::accueil.html.twig" %}*/
/* {% block body %}*/
/* <div id="container">*/
/* <div class="row">*/
/*         {% for message in app.session.flashbag.get('info') %}*/
/*         <div class="card-panel light-green white-text"><i class="fa fa-check-circle" aria-hidden="true"></i>*/
/* {{message}}</div>*/
/*         */
/*       {% endfor %}*/
/*       <h2>Ajouter une tablette</h2>*/
/*       <form action="{{path('pg_gsb_frais_creertablette')}}" method="POST" id="add-tablette">*/
/*           */
/*           <div class="input-field col m3">*/
/*           <select id="os-select" name ="os" form="add-tablette">*/
/*           <option value="Android">Android</option>*/
/*           <option value="OSX">OSX</option>*/
/*           </select>*/
/*           </div>*/
/* <div class="input-field col m3">*/
/*           <input type="text" name ="memoireinterne" placeholder="Mémoire interne"/>*/
/*           </div>*/
/*           <div class="input-field col m3">*/
/*           <input type ="text" id="memoire-ext" name ="memoireext" placeholder="Mémoire externe"/>*/
/*           </div>*/
/*           <div class="input-field col m3">*/
/*           */
/*             <input class="btn blue waves-effect" type="submit" value="Valider" size="20" />*/
/*           </div>*/
/*           </div>*/
/*           </div>*/
/*       </form>*/
/* </div>*/
/* {% endblock %}*/
