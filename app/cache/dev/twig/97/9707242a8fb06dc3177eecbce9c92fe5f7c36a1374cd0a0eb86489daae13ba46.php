<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_2808ef651750b0ce03d57c33124ba48de4a43d3b1e516aebbf46fcce534457a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1f1b3eb1b892d83db4607f6cf5ad6550c53cf29ced0a99158a5ccac72697658d = $this->env->getExtension("native_profiler");
        $__internal_1f1b3eb1b892d83db4607f6cf5ad6550c53cf29ced0a99158a5ccac72697658d->enter($__internal_1f1b3eb1b892d83db4607f6cf5ad6550c53cf29ced0a99158a5ccac72697658d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_1f1b3eb1b892d83db4607f6cf5ad6550c53cf29ced0a99158a5ccac72697658d->leave($__internal_1f1b3eb1b892d83db4607f6cf5ad6550c53cf29ced0a99158a5ccac72697658d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
