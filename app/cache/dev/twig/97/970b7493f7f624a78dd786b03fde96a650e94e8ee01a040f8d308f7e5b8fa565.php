<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_41907e7a5367b26cca058ef90a0d4c00e21ae6b7f0584f20aa76db461ac8cb05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f02262578e8924a6ca0479fb0900b883fbfaff59835d9c6d74939b2ff34c7534 = $this->env->getExtension("native_profiler");
        $__internal_f02262578e8924a6ca0479fb0900b883fbfaff59835d9c6d74939b2ff34c7534->enter($__internal_f02262578e8924a6ca0479fb0900b883fbfaff59835d9c6d74939b2ff34c7534_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_f02262578e8924a6ca0479fb0900b883fbfaff59835d9c6d74939b2ff34c7534->leave($__internal_f02262578e8924a6ca0479fb0900b883fbfaff59835d9c6d74939b2ff34c7534_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
