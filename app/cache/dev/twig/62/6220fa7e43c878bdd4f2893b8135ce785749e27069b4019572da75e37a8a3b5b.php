<?php

/* PgGsbFraisBundle:Home:tabletteaffecter.html.twig */
class __TwigTemplate_d896f73a4f12ac1a2cbbc5299f8ab38bc1bb7834d596e853f121956d2c1bf116 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PgGsbFraisBundle::accueil.html.twig", "PgGsbFraisBundle:Home:tabletteaffecter.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'block1' => array($this, 'block_block1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PgGsbFraisBundle::accueil.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a460d237b59fe9182da77b07abd41c6199b6c21db5b8c1bfdacb3b3cc98f4ddd = $this->env->getExtension("native_profiler");
        $__internal_a460d237b59fe9182da77b07abd41c6199b6c21db5b8c1bfdacb3b3cc98f4ddd->enter($__internal_a460d237b59fe9182da77b07abd41c6199b6c21db5b8c1bfdacb3b3cc98f4ddd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PgGsbFraisBundle:Home:tabletteaffecter.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a460d237b59fe9182da77b07abd41c6199b6c21db5b8c1bfdacb3b3cc98f4ddd->leave($__internal_a460d237b59fe9182da77b07abd41c6199b6c21db5b8c1bfdacb3b3cc98f4ddd_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_db5805262e1807d8ee195a00e6f039bee4303a8db89fd4e1b3a2725cd346adaf = $this->env->getExtension("native_profiler");
        $__internal_db5805262e1807d8ee195a00e6f039bee4303a8db89fd4e1b3a2725cd346adaf->enter($__internal_db5805262e1807d8ee195a00e6f039bee4303a8db89fd4e1b3a2725cd346adaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<h1>Listes des tablettes affectées</h1>


";
        // line 6
        $this->displayBlock('block1', $context, $blocks);
        
        $__internal_db5805262e1807d8ee195a00e6f039bee4303a8db89fd4e1b3a2725cd346adaf->leave($__internal_db5805262e1807d8ee195a00e6f039bee4303a8db89fd4e1b3a2725cd346adaf_prof);

    }

    public function block_block1($context, array $blocks = array())
    {
        $__internal_2014ae4eb0d0263ad5a0790ba483210e6690231a2d4693857f2e5447d23b56dc = $this->env->getExtension("native_profiler");
        $__internal_2014ae4eb0d0263ad5a0790ba483210e6690231a2d4693857f2e5447d23b56dc->enter($__internal_2014ae4eb0d0263ad5a0790ba483210e6690231a2d4693857f2e5447d23b56dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block1"));

        // line 7
        echo "<table class=\"highlight responsive-table centered\">
<thead>

<th data-field=\"utilisateur\"> Utilisateur</th>
<th data-field=\"ostab\">OS</th>
<th data-field=\"memoiretab\">Mémoire</th>
<th data-field=\"memoireext\">Mémoire Ext</th>
<th data-field=\"idtab\">ID Tab </th>
<th data-field=\"supprtab\">Retirer</th>
</thead>
<tbody>
      ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabletteAffecte"]) ? $context["tabletteAffecte"] : $this->getContext($context, "tabletteAffecte")));
        foreach ($context['_seq'] as $context["_key"] => $context["tablette"]) {
            // line 19
            echo "      <tr>


      <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["tablette"], "refVisiteur", array()), "html", null, true);
            echo "</td>
      <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["tablette"], "osTablette", array()), "html", null, true);
            echo "</td>
      <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["tablette"], "memoireTab", array()), "html", null, true);
            echo "Go</td>
      ";
            // line 25
            if (($this->getAttribute($context["tablette"], "memoireExtTab", array()) == null)) {
                // line 26
                echo "      

      <td><i class=\"fa fa-times red-text z-index-1\" aria-hidden=\"true\"></i>
</td>

      ";
            } else {
                // line 32
                echo "
      <td>";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute($context["tablette"], "memoireExtTab", array()), "html", null, true);
                echo "</td>

      ";
            }
            // line 36
            echo "      <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tablette"], "idTab", array()), "html", null, true);
            echo "</td>
      <td><form action=\"";
            // line 37
            echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_retirertablette");
            echo "\" method=\"POST\">
      <input type=\"hidden\" name=\"id\" value=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["tablette"], "idTab", array()), "html", null, true);
            echo "\"/>
      <button type=\"submit\" class=\"btn red hide-on-small-only\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i><span class=\"hide-on-small-only\"> Retirer</span></button>
      <button type=\"submit\" class=\"btn red hide-on-med-and-up\" style=\"padding:0 10px;\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></button>
 
      </form>
      
      
</tr>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tablette'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "
</tbody>
</table>  
";
        
        $__internal_2014ae4eb0d0263ad5a0790ba483210e6690231a2d4693857f2e5447d23b56dc->leave($__internal_2014ae4eb0d0263ad5a0790ba483210e6690231a2d4693857f2e5447d23b56dc_prof);

    }

    public function getTemplateName()
    {
        return "PgGsbFraisBundle:Home:tabletteaffecter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 47,  120 => 38,  116 => 37,  111 => 36,  105 => 33,  102 => 32,  94 => 26,  92 => 25,  88 => 24,  84 => 23,  80 => 22,  75 => 19,  71 => 18,  58 => 7,  46 => 6,  41 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends "PgGsbFraisBundle::accueil.html.twig" %}*/
/* {% block body %}*/
/* <h1>Listes des tablettes affectées</h1>*/
/* */
/* */
/* {% block block1 %}*/
/* <table class="highlight responsive-table centered">*/
/* <thead>*/
/* */
/* <th data-field="utilisateur"> Utilisateur</th>*/
/* <th data-field="ostab">OS</th>*/
/* <th data-field="memoiretab">Mémoire</th>*/
/* <th data-field="memoireext">Mémoire Ext</th>*/
/* <th data-field="idtab">ID Tab </th>*/
/* <th data-field="supprtab">Retirer</th>*/
/* </thead>*/
/* <tbody>*/
/*       {% for tablette in tabletteAffecte %}*/
/*       <tr>*/
/* */
/* */
/*       <td>{{ tablette.refVisiteur }}</td>*/
/*       <td>{{ tablette.osTablette }}</td>*/
/*       <td>{{ tablette.memoireTab }}Go</td>*/
/*       {% if tablette.memoireExtTab == NULL %}*/
/*       */
/* */
/*       <td><i class="fa fa-times red-text z-index-1" aria-hidden="true"></i>*/
/* </td>*/
/* */
/*       {% else %}*/
/* */
/*       <td>{{ tablette.memoireExtTab }}</td>*/
/* */
/*       {% endif %}*/
/*       <td>{{ tablette.idTab }}</td>*/
/*       <td><form action="{{path('pg_gsb_frais_retirertablette')}}" method="POST">*/
/*       <input type="hidden" name="id" value="{{tablette.idTab}}"/>*/
/*       <button type="submit" class="btn red hide-on-small-only"><i class="fa fa-trash" aria-hidden="true"></i><span class="hide-on-small-only"> Retirer</span></button>*/
/*       <button type="submit" class="btn red hide-on-med-and-up" style="padding:0 10px;"><i class="fa fa-trash" aria-hidden="true"></i></button>*/
/*  */
/*       </form>*/
/*       */
/*       */
/* </tr>*/
/*       {% endfor %}*/
/* */
/* </tbody>*/
/* </table>  */
/* {% endblock %}*/
/* {% endblock %}*/
