<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_bf80c39d0c8458667d7c5699ad1a217866c9769961442b6f35bff82d646a7dd6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a257544dea8b85663b7be85a283d6921e2a44f2da8e54919eeaea99d8f5f8933 = $this->env->getExtension("native_profiler");
        $__internal_a257544dea8b85663b7be85a283d6921e2a44f2da8e54919eeaea99d8f5f8933->enter($__internal_a257544dea8b85663b7be85a283d6921e2a44f2da8e54919eeaea99d8f5f8933_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_a257544dea8b85663b7be85a283d6921e2a44f2da8e54919eeaea99d8f5f8933->leave($__internal_a257544dea8b85663b7be85a283d6921e2a44f2da8e54919eeaea99d8f5f8933_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
