<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_83cb46cffeab171f48035adad8332e48d79e8045b92b8ec2ae5478b865427c7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9e4a2bacc3febd69bcbce6961f1754f0cbd5f8e3be273ba62d6d788040355a18 = $this->env->getExtension("native_profiler");
        $__internal_9e4a2bacc3febd69bcbce6961f1754f0cbd5f8e3be273ba62d6d788040355a18->enter($__internal_9e4a2bacc3febd69bcbce6961f1754f0cbd5f8e3be273ba62d6d788040355a18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_9e4a2bacc3febd69bcbce6961f1754f0cbd5f8e3be273ba62d6d788040355a18->leave($__internal_9e4a2bacc3febd69bcbce6961f1754f0cbd5f8e3be273ba62d6d788040355a18_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
