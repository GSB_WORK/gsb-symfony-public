<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_cb85af5755488727f28028d3a17b4cd3c241b37711a00e6492fee6105ce17bff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b408518af436120a539777b717840c8ddc04721ea18130751219429fca8fb2c5 = $this->env->getExtension("native_profiler");
        $__internal_b408518af436120a539777b717840c8ddc04721ea18130751219429fca8fb2c5->enter($__internal_b408518af436120a539777b717840c8ddc04721ea18130751219429fca8fb2c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_b408518af436120a539777b717840c8ddc04721ea18130751219429fca8fb2c5->leave($__internal_b408518af436120a539777b717840c8ddc04721ea18130751219429fca8fb2c5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
