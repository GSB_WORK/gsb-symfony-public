<?php

/* PgGsbFraisBundle::accueil.html.twig */
class __TwigTemplate_74037d1b4dec65747987b346531aa5f72a56d3cbaff93a8b4b266dac47490db3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PgGsbFraisBundle::layout.html.twig", "PgGsbFraisBundle::accueil.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'block_menu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PgGsbFraisBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fae1bd8a7a6bb48878ee6d5e61b84233339e573930e3a92d0e75110d0c9d30f7 = $this->env->getExtension("native_profiler");
        $__internal_fae1bd8a7a6bb48878ee6d5e61b84233339e573930e3a92d0e75110d0c9d30f7->enter($__internal_fae1bd8a7a6bb48878ee6d5e61b84233339e573930e3a92d0e75110d0c9d30f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PgGsbFraisBundle::accueil.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fae1bd8a7a6bb48878ee6d5e61b84233339e573930e3a92d0e75110d0c9d30f7->leave($__internal_fae1bd8a7a6bb48878ee6d5e61b84233339e573930e3a92d0e75110d0c9d30f7_prof);

    }

    // line 2
    public function block_menu($context, array $blocks = array())
    {
        $__internal_7aecae129922e94b7179a21d3d097118df32b052f20e397e02d4f83416487254 = $this->env->getExtension("native_profiler");
        $__internal_7aecae129922e94b7179a21d3d097118df32b052f20e397e02d4f83416487254->enter($__internal_7aecae129922e94b7179a21d3d097118df32b052f20e397e02d4f83416487254_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 3
        echo "<div id=\"menuGauche\">
     <div id=\"infosUtil\">
         <h5>
            ";
        // line 6
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array(), "any", false, true), "get", array(0 => "nom"), "method", true, true)) {
            // line 7
            echo "                          Utilisateur: ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "nom"), "method"), "html", null, true);
            echo "<br>   
            ";
        }
        // line 9
        echo "        </h5>
     </div>  
        <ul id=\"menuList\">
\t   <li class=\"smenu\">
              <a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_saisirfrais");
        echo "\" title=\"Saisie fiche de frais \">Saisie fiche de frais</a>
           </li>
           <li class=\"smenu\">
              <a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_listefrais");
        echo "\" title=\"Consultation de mes fiches de frais\">Mes fiches de frais</a>
           </li>\t
           <li class=\"smenu\">
              <a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_validerfraishorsforfais");
        echo "\" title=\"valider les frais\">Valider les frais</a>
           </li>
           <li class=\"smenu\">
              <a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_affichertabletteaffecter");
        echo "\" title=\"Afficher les tablettes\">Afficher les affectations</a>
           </li>
           <li class=\"smenu\">
              <a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_creertablette");
        echo "\" title=\"Créer une tablette\">Créer une tablette</a>
           </li>
           <li class=\"smenu\">
              <a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("pg_gsb_frais_deconnexion");
        echo "\" title=\"Se déconnecter\">Déconnexion</a>
           </li>
         </ul>
     </div> 
";
        
        $__internal_7aecae129922e94b7179a21d3d097118df32b052f20e397e02d4f83416487254->leave($__internal_7aecae129922e94b7179a21d3d097118df32b052f20e397e02d4f83416487254_prof);

    }

    public function getTemplateName()
    {
        return "PgGsbFraisBundle::accueil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 28,  83 => 25,  77 => 22,  71 => 19,  65 => 16,  59 => 13,  53 => 9,  47 => 7,  45 => 6,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "PgGsbFraisBundle::layout.html.twig" %}*/
/* {% block menu %}*/
/* <div id="menuGauche">*/
/*      <div id="infosUtil">*/
/*          <h5>*/
/*             {%if(app.session.get('nom') is defined )%}*/
/*                           Utilisateur: {{app.session.get('nom')}}<br>   */
/*             {%endif%}*/
/*         </h5>*/
/*      </div>  */
/*         <ul id="menuList">*/
/* 	   <li class="smenu">*/
/*               <a href="{{path('pg_gsb_frais_saisirfrais')}}" title="Saisie fiche de frais ">Saisie fiche de frais</a>*/
/*            </li>*/
/*            <li class="smenu">*/
/*               <a href="{{path('pg_gsb_frais_listefrais')}}" title="Consultation de mes fiches de frais">Mes fiches de frais</a>*/
/*            </li>	*/
/*            <li class="smenu">*/
/*               <a href="{{path('pg_gsb_frais_validerfraishorsforfais')}}" title="valider les frais">Valider les frais</a>*/
/*            </li>*/
/*            <li class="smenu">*/
/*               <a href="{{path('pg_gsb_frais_affichertabletteaffecter')}}" title="Afficher les tablettes">Afficher les affectations</a>*/
/*            </li>*/
/*            <li class="smenu">*/
/*               <a href="{{path('pg_gsb_frais_creertablette')}}" title="Créer une tablette">Créer une tablette</a>*/
/*            </li>*/
/*            <li class="smenu">*/
/*               <a href="{{path('pg_gsb_frais_deconnexion')}}" title="Se déconnecter">Déconnexion</a>*/
/*            </li>*/
/*          </ul>*/
/*      </div> */
/* {% endblock%}*/
/* */
/* */
/* {#*/
/*  <li class="smenu">*/
/*               <a href="{{path('pg_gsb_frais_saisirfrais')}}" title="Saisie fiche de frais ">Saisie fiche de frais</a>*/
/*            </li>*/
/*            <li class="smenu">*/
/*               <a href="{{path('pg_gsb_frais_listefrais')}}" title="Consultation de mes fiches de frais">Mes fiches de frais</a>*/
/*            </li>*/
/* #}*/
