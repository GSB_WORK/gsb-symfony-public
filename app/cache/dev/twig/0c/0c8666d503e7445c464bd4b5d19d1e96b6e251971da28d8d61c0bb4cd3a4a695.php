<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_18ffe1c0de766ac96d2a1fcd3fa7a5f48d0ccea892d995bc7bb19270e75bbc85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9a046894b7d7dee6e7de9d7185c3581f16695bd7d1dbb4f9da339c418e602cb = $this->env->getExtension("native_profiler");
        $__internal_a9a046894b7d7dee6e7de9d7185c3581f16695bd7d1dbb4f9da339c418e602cb->enter($__internal_a9a046894b7d7dee6e7de9d7185c3581f16695bd7d1dbb4f9da339c418e602cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_a9a046894b7d7dee6e7de9d7185c3581f16695bd7d1dbb4f9da339c418e602cb->leave($__internal_a9a046894b7d7dee6e7de9d7185c3581f16695bd7d1dbb4f9da339c418e602cb_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
