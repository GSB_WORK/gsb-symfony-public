<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_bf58abc06637dbc7c4a3b719dbb7d561b08c5682b0342af6231021d5fdfdd3c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_55be90dae8d8ceb5ff5c02e4073a35b2d29169824c74e430cb6099e0a5f3b0c1 = $this->env->getExtension("native_profiler");
        $__internal_55be90dae8d8ceb5ff5c02e4073a35b2d29169824c74e430cb6099e0a5f3b0c1->enter($__internal_55be90dae8d8ceb5ff5c02e4073a35b2d29169824c74e430cb6099e0a5f3b0c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_55be90dae8d8ceb5ff5c02e4073a35b2d29169824c74e430cb6099e0a5f3b0c1->leave($__internal_55be90dae8d8ceb5ff5c02e4073a35b2d29169824c74e430cb6099e0a5f3b0c1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
