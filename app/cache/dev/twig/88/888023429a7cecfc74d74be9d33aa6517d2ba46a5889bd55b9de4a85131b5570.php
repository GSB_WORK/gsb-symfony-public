<?php

/* base.html.twig */
class __TwigTemplate_12131cd67860b90e62223d675551fec039f3a3a146afeabe92af56f275eb4749 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1de4433471464b36952d4cf4102b42b91c6329f680a1e340477e3a172b13454b = $this->env->getExtension("native_profiler");
        $__internal_1de4433471464b36952d4cf4102b42b91c6329f680a1e340477e3a172b13454b->enter($__internal_1de4433471464b36952d4cf4102b42b91c6329f680a1e340477e3a172b13454b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_1de4433471464b36952d4cf4102b42b91c6329f680a1e340477e3a172b13454b->leave($__internal_1de4433471464b36952d4cf4102b42b91c6329f680a1e340477e3a172b13454b_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_804264b2e08d00508decf3939f028df902c6e7331fb8b86454dfd2059b482ada = $this->env->getExtension("native_profiler");
        $__internal_804264b2e08d00508decf3939f028df902c6e7331fb8b86454dfd2059b482ada->enter($__internal_804264b2e08d00508decf3939f028df902c6e7331fb8b86454dfd2059b482ada_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_804264b2e08d00508decf3939f028df902c6e7331fb8b86454dfd2059b482ada->leave($__internal_804264b2e08d00508decf3939f028df902c6e7331fb8b86454dfd2059b482ada_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_dd47e2785817bdf3b26ef49c5da9a6f8983ff9e0077d13ec7308e292e05d8182 = $this->env->getExtension("native_profiler");
        $__internal_dd47e2785817bdf3b26ef49c5da9a6f8983ff9e0077d13ec7308e292e05d8182->enter($__internal_dd47e2785817bdf3b26ef49c5da9a6f8983ff9e0077d13ec7308e292e05d8182_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_dd47e2785817bdf3b26ef49c5da9a6f8983ff9e0077d13ec7308e292e05d8182->leave($__internal_dd47e2785817bdf3b26ef49c5da9a6f8983ff9e0077d13ec7308e292e05d8182_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_6fb0a67badd4c2b1f712457e1069a7b79873f4bfbefa3bfe716a0036f25f626f = $this->env->getExtension("native_profiler");
        $__internal_6fb0a67badd4c2b1f712457e1069a7b79873f4bfbefa3bfe716a0036f25f626f->enter($__internal_6fb0a67badd4c2b1f712457e1069a7b79873f4bfbefa3bfe716a0036f25f626f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_6fb0a67badd4c2b1f712457e1069a7b79873f4bfbefa3bfe716a0036f25f626f->leave($__internal_6fb0a67badd4c2b1f712457e1069a7b79873f4bfbefa3bfe716a0036f25f626f_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_297fab34b6cf7c27660d2b4013664b34485cf29603d1085543810fe6a94ebbf6 = $this->env->getExtension("native_profiler");
        $__internal_297fab34b6cf7c27660d2b4013664b34485cf29603d1085543810fe6a94ebbf6->enter($__internal_297fab34b6cf7c27660d2b4013664b34485cf29603d1085543810fe6a94ebbf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_297fab34b6cf7c27660d2b4013664b34485cf29603d1085543810fe6a94ebbf6->leave($__internal_297fab34b6cf7c27660d2b4013664b34485cf29603d1085543810fe6a94ebbf6_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
